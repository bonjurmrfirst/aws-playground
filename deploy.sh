#!/bin/bash
su - ubuntu

echo "exec"
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

echo "apt update -y"
apt update -y

echo "apt install nodejs -y"
apt install nodejs -y

echo "apt install npm -y"
apt install npm -y

echo "npm install pm2 -g"
npm install pm2 -g

sudo env PATH=$PATH:/usr/bin /usr/local/lib/node_modules/pm2/bin/pm2 startup systemd -u ubuntu --hp /home/ubuntu

echo "pm2 start /aws-playground/index.js"
nohup pm2 start /aws-playground/index.js

echo "npm install forever -g"
npm install forever -g

echo "forever start"
nohup forever start /aws-playground/index.js